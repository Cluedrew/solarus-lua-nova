-- Detailed Lua Nova Interface
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- If you want to use Lua Nova along side your own scripting or if the set up
-- global-use does not work in your project. See the Lua API documentation.

local tools = require(string.gsub(..., "lua[-]nova$", "core/tools"))
local dialog = tools.require"dialog"
local engine = tools.require"engine"
local settings = tools.require"settings"

local lua_nova = {}

lua_nova.run = engine.run

lua_nova.enable_dialog = dialog.enable_dialog

-- Now there should be no more writes that aren't settings.
setmetatable(lua_nova, {
    __index = settings,
    __newindex = settings._user_set,
})

return lua_nova
