# Lua Nova

A Lua library for use in [Solarus](https://www.solarus-games.org/) quests. It
is made to help content creation without using Lua scripting. To this end it
allows you to use other parts of the Solarus Quest Editor to do scripting.

Note that Lua Nova is not intended to completely replace Lua scripting. It
does not allow arbitrary programming and in particular it is not trying to
let you make things you can easily import from other resource packs, like
items and enemies.

If you want to see Lua Nova in action, check out the [Lua Nova Showcase](
https://gitlab.com/Cluedrew/solarus-lua-nova-showcase/) a Solarus quest built
to help test and develop the library.

*Lua Nova is still in its early days. The interface is not stable. Feel free
to reach out on the Solarus Discord or here if you have feedback.*

## Usage

Most of Lua Nova is used through the Solarus Quest Editor. Tutorials about
the editor still apply, except that Lua scripting can be (in part) ignored
and Lua Nova will read user properties that the engine does not use.

To use Lua Nova copy this directory into your quest. You can put it anywhere
and even change the directory's name as long as everything in it remains in
the same position. Then require one of the two interface files on start-up:

+   The simple interface in `global-use` is for when you are avoiding
    scripting as much as possible. It activates every feature everywhere it
    can and does its best to detect and work with your set-up.

    If you use this interface make sure to delete everything in your map
    scripts. In fact if you put much of anything in there, such as an
    `on_started` event, you and Lua Nova will start stepping on each other's
    toes.

+   The detailed interface in `lua-nova` is for when you want to use it with
    other scripting. This gives you more control over when and how Lua Nova
    does its work.

    No automatic set-up is preformed. See the Lua API documentation for the
    interface to this module.

See the `docs` directory for more information. Including the listings of all
the user properties Lua Nova uses.

## License

Lua Nova is licensed under GNU Lesser General Public License version 3. You
may distribute it under that license or a later version of it.

It is intended that you may use Lua Nova in any Solarus project as long as you
respect that the library itself is free-software. If you include the license
files, use the documented interface and make any changes avalible under the
license then everything should be fine. For the actual legal details, please
read the licenses.
