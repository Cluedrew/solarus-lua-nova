# Editor Programming

This is a reference document for Lua Nova features that can be used without
any Lua scripting (after the initial set up). Most are used from the map
editor or dialog editor.

## Entity Hooks

Hooks are the primary way of interacting with Lua Nova. All map entities in
Solarus can have user properties (key value pairs) on them. Hooks are special
keys that Lua Nova recognizes and uses to add extra behaviour to the map.

### arena_entrance
-   Available On: Doors, Dynamic Tiles
-   Value: Arena Name (see Arenas)

This is an entrance to an arena. When the arena is sealed doors will be closed
and dynamic tiles will be enabled. When the arena is unsealed doors will be
opened and dynamic tiles will be disabled.

### arena_foe
-   Available On: Enemies
-   Value: Arena Name (see Arenas)

Marks the entity as one of the foes in the arena. These foes must all be
defeated to unseal the arena

### arena_sensor
-   Available On: Sensors
-   Value: Arena Name (see Arenas)

This sensor will seal the arena when it activates.

### dialog
-   Available On: NPCs.
-   Value: Dialog Id or [VARIABLE:]VALUE_TO_DIALOG_ID

Allows you to dynamically choose which dialog is started when the hero
interacts with the NPC. The NPC's Action must be set to "Call the map script"
for this to work. This hook can also be used to start a fixed dialog if the
action must be set to the map script for some other reason or if the dialog
uses Lua Nova's dialog hooks, in which case the value can be a single dialog
id. The value for a dynamic look up is more complex.

VARIABLE may be a savegame variable (starts with a letter and consists
of letters, numbers and underscores) or a special variable (listed below). If
not given the name of the entity the hook is on can be used as the variable
name. It is an error if neither are provided.

VALUE_TO_DIALOG_ID is a comma seperated list. One of these elements may be a
lone dialog id, this will be the default dialog. The others should be pairs
seperated by an equal sign. The first element is the key and the second is
a dialog id. The value of the VARIABLE is converted to a string and checked
against the keys. If it matches one the paired dialog is use, otherwise the
default dialog is used. It is an error if the default dialog is not provided
and the value does not match any of the keys. Do not use commas (`,`) or
equal signs (`=`) in the keys or dialog ids.

#### Special Variables
(Special Variables are experimental and below is just the proof-of-concept.)
-   *!is_hero_max_life*: True if the hero's life is the same as their maximum
    life, false otherwise.

### disable_when/disable_while
-   Available On: All Entities
-   Value: Trigger (see Triggers)

Both variants disable the entity when the trigger activates, the while variant
also enables the entity when the trigger deactivates.

### enable_when/enable_while
-   Available On: All Entities
-   Value: Trigger (see Triggers)

Both variants enable the entity when the trigger activates, the while variant
also disables the entity when the trigger deactivates.

### on_activated
-   Available On: Sensors
-   Value: Action List (see Actions)

When the sensor is activated (the hero is first completely within it) all the
actions in the action list will be executed.

### open_when/open_while
-   Available On: Doors
-   Value: Trigger (see Triggers)

Both variants open the door when the trigger activates, the while variant also
closes the door when the trigger deactivates.

### open_when_camera
-   Available On: Doors
-   Value: Trigger (see Triggers)

As open_when put pans the camera over to the door, opens it, and then pans
the camera back.

### start_song
-   Available On: Sensors
-   Value: Song Id

When the sensor is activated, start playing the song.

### set_hero_layer
-   Available On: Sensors
-   Value: An integer, optionally proceed by =, + or -.

When the sensor is activated set the hero's layer. With a + or - the new layer
is relative to the current layer (above or below respectively), otherwise the
new layer is just the number.

### set_solid_ground
-   Available On: Sensors
-   Value: "save" or "reset"

You can either save (the the hero's current position) or reset the solid
ground when the sensor is activated.

## Dialog Hooks

This is the second group of hooks. These are available in the dialog editor,
under dialog properties. They are similar to entity hooks in their key value
format but are only available here.

Note that Lua Nova does not actually add anything to display dialog. It will
use the built-in display and never set an exit status. If you want more than
that add a separate dialog system. It should also document what status codes
it uses when it ends a dialog.

Hooks are checked and run in the order listed here.

### always
-    Value: Action List

After the dialog is finished all actions in the list are preformed.

### if_*
-    Value: Action List

The star in `if_*` is the exit status of the dialog converted to a string.
Any number of different hooks matching this form may be used; however only the
one that matches the current exit status is used. All actions in it are run in
order.

### ifelse
-   Value: Action List

If no `if_*` is matched and run then the actions in this hook will be run if
it is given.

## Triggers

Triggers are used as the value of hooks that specify what should happen but
not when. Triggers are used to specify when. When a trigger is used as a value
it will always have one of the two formats "NAME" if you are not providing an
extra value and "NAME:VALUE" to provide an additional argument.

Triggers will either be one-sided or two-sided. One-sided triggers activate
when their condition becomes true and then stay active. Two-sided triggers
activate the same way and deactivate when the condition becomes false and
can go back and forth.

By convention, hooks that use triggers have either "when" or "while" in their
name. Those with "when" accept one-sided or two-sided triggers (but will not
do anything when the trigger deactivates). Those with "while" accept two-sided
triggers and will do something on both the activate and deactivate.

### clear_enemies
-   Value: Optional Name Prefix
-   One Sided

The trigger activates when all enemies in the map are dead. Or if the name
prefix is supplied, all entities in the map with that name prefix are dead.

### switches
-   Value: Name Prefix
-   Two Sided

The trigger activates when all entities with that name prefix, which must be
switches, are active. It will deactivate again when any of the switches become
deactivate again.

## Actions

There are some places where Lua Nova will execute a series of actions. These
are the actions it understands.

An action list is a series of actions separated by semi-colons. Each action is
given as "NAME:VALUE", the action's name, a colon and then the extra value. If
you don't want to give the value you can omit both the colon and the value.
However many actions require a value.

### give_item
Value is ITEM[,[VARIANT][,[AMOUNT]]].

Give the ITEM (an item id) to the player.

If VARIANT is given they will be given that variant of the item (or take away
the item if it is 0), otherwise if they do not have the item they will be
given variant 1.

If AMOUNT is given the item must have an amount. The item's amount will be
increased by AMOUNT.

### set
Value is a comma separated list of key=value pairs. They keys are save-game
variables and will be assigned to the "unstringified" value.

An "unstringified" value is the input string unless it is "nil", "true",
"false" or a decimal integer. Then it is converted to the appropriate Lua
value.

### teleport
Value is [MAP][,[DESTINATION][,TRANSITION]]. That is there are three values
separated by commas, all three values are optional as are the commas after
the last provided value.

MAP defaults to the current map. DESTINATION will be the default destination
if not given and the TRANSITION is a fade-out by default. Actually the
transition comes from `game:get_transition_style()` which is a fade-out by
default.

## Arenas

Arenas are sets of entities designed to lock the hero in a room with some
enemies, such as for a boss or mini-boss fight. Arenas are created implicitly
the first time a hook refers to them.

Arenas start open and will seal when one of their sensors is activated and
close all entrances to the arena. They will then unseal when all the foes
have been defeated and all entrances will open.

See hooks: `arena_entrance`, `arena_foe` and `arena_sensor`.
