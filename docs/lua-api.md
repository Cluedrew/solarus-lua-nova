# Lua API

When you include the detailed interface in `lua-nova.lua` the script will
return a table, which here will be called `lua_nova` but you may name anything
you like.

Remember that is you are using the simple interface in `global-use.lua` you
may ignore all of this. It will set up the system to call functions at the
approprate time and will even check the environment to try and configure
everything properly. This is only nessasary if that doesn't work.

## Functions

### enable_dialog
enable_dialog(game)

Enables Lua Nova's dialog system on that game. Note that to start a dialog
that works with Lua Nova you must still use the `start_dialog` function.

### run
run(map)

Run Lua Nova on a map. This will scan the entire map, every entity on the map,
and every property on each entity. All hooks found will be installed
immediately.

### start_dialog
start_dialog(game, dialog_id, info, callback)

Starts a dialog compatable with Lua Nova. It has the same interfaces as
`game:start_dialog(dialog_id, info, callback)`.

Note that this is also a setting.

## Settings

There are several values in lua_nova that the system will check and use. These
can be used to configure Lua Nova to better fit your project.

These are the only values you should write in lua_nova. Do not use `rawget` or
`rawset` to read or write them.

### register_event
Type: function(object, event_name, callback)

If set Lua Nova will call this function instead of assigning an event.

### start_dialog
Type: function(game, dialog_id, info, callback)

You can also override `start_dialog` to add any additional features. If you
do make sure to call the original to do the final start.

Note that this is also a function you can use.
