-- Lua Nova Internal File
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- Configuration settings for Lua Nova.
-- Except for "_user_set", which is a checker for user access, everything is
-- just a setting that can be read/written to by the rest of Lua Nova. See the
-- keys table for a description of the different settings.

local tools = require(string.gsub(..., "/settings$", "/tools"))

local settings = {}

local keys = {
    -- Function used to add events to objects.
    register_event = "function",
    -- Function used to start_dialogs compatable with Lua Nova.
    start_dialog = "function",
}

function settings._user_set(_self, key, value)
    local key_type = keys[key]
    if not key_type then
        tools.error{"Bad settings key: ", key, level = 2}
    end
    local value_type = sol.main.get_type(value)
    if "nil" == value_type or key_type == value_type then
        settings[key] = value
    else
        tools.error{"Bad settings type on key ", key, " expected a ",
            key_type, " but got a ", value_type, level = 2}
    end
end

return settings
