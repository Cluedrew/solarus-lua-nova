-- Lua Nova Internal File
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- Defines all the property keys recognized by Lua Nova, called hooks.
--
-- They can all be laid out like this because no two should have the same name
-- even if they would not otherwise conflict just for clarity reasons.

local tools = require(string.gsub(..., "/hooks$", "/tools"))
local settings = tools.require"settings"

local hooks = {}

local triggers = {
    clear_enemies = {
        set = function(engine, entity, value, on_begin)
            local map = engine.map
            local function on_dead(self)
                if not map:has_entities(value) then
                    on_begin()
                end
            end
            for enemy in map:get_entities(value) do
                engine:store_event(enemy, "on_dead", on_dead)
            end
        end,
    },
    switches = {
        supports_end = true,
        set = function(engine, entity, value, on_begin, on_end)
            local switches_left = 0
            local function on_activated(self)
                switches_left = switches_left - 1
                if 0 == switches_left then
                    on_begin()
                end
            end
            local function on_inactivated(self)
                if on_end and 0 == switches_left then
                    on_end()
                end
                switches_left = switches_left + 1
            end
            for switch in engine.map:get_entities(value) do
                switches_left = switches_left + 1
                engine:store_event(switch, "on_activated", on_activated)
                engine:store_event(switch, "on_inactivated", on_inactivated)
            end
        end,
    },
}

local function trigger_common(engine, entity, value, on_begin, on_end)
    local trigger_name, trigger_value = string.match(value, "^([^:]*):(.*)$")
    if not trigger_name then trigger_name = value end
    local data = triggers[trigger_name]
    if not data then
        error("[Lua Nova] No such trigger '" .. trigger_name .. "'")
    elseif on_end and not data.supports_end then
        error("[Lua Nova] Trigger " .. trigger_name
            .. " does not support while.")
    end
    data.set(engine, entity, trigger_value, on_begin, on_end)
end

local function enable_common(engine, entity, value, is_two_sided)
    local function on_begin()
        entity:set_enabled(true)
    end
    local on_end = nil
    if is_two_sided then
        function on_end()
            entity:set_enabled(false)
        end
    end
    trigger_common(engine, entity, value, on_begin, on_end)
end

local function disable_common(engine, entity, value, is_two_sided)
    local function on_begin()
        entity:set_enabled(false)
    end
    local on_end = nil
    if is_two_sided then
        function on_end()
            entity:set_enabled(true)
        end
    end
    trigger_common(engine, entity, value, on_begin, on_end)
end

local function open_common(engine, entity, value, is_two_sided)
    local function on_begin()
        entity:open()
    end
    local on_end = nil
    if is_two_sided then
        function on_end()
            entity:close()
        end
    end
    trigger_common(engine, entity, value, on_begin, on_end)
end

local function trigger_when(f)
    return function(engine, entity, value)
        return f(engine, entity, value, false)
    end
end

local function trigger_while(f)
    return function(engine, entity, value)
        return f(engine, entity, value, true)
    end
end

local actions = {}

function actions.set(mode, game, data)
    local new_values = {}
    -- This might break silently.
    for key, value in string.gmatch(data, ",*([^,=]*)=([^,]*)") do
        -- TODO: check is valid Lua key.
        new_values[key] = tools.fromstring(value)
    end

    return mode(game, function(game, values)
        for key, value in pairs(values) do
            game:set_value(key, value)
        end
    end, new_values)
end

function actions.give_item(mode, game, value)
    if not value then
        tools.error"No item_id provided to give_item."
    end

    local iter = string.gmatch(value, "[^,]*")
    local item_id = iter()
    local variant = tonumber(iter() or "")
    local amount = tonumber(iter() or "")
    local item = game:get_item(item_id)

    if not item then
        tools.error{"No such item: ", item_id}
    elseif amount and not item:has_amount() then
        tools.error{"The item ", item_id, " has no amount."}
    end

    return mode(game, function(game, item, variant, amount)
        if variant then
            item:set_variant(variant)
        elseif 0 == item:get_variant() then
            item:set_variant(1)
        end
        if amount then
            item:add_amount(amount)
        end
    end, item, variant, amount)
end

function actions.teleport(mode, game, value)
    local args = {}
    for arg in (value or ""):gmatch("[^,]*") do
        table.insert(args, arg)
    end

    if "" == args[1] then
        args[1] = game:get_map():get_id()
    end
    if "" == args[2] then
        args[2] = nil
    end
    if "" == args[3] then
        args[3] = nil
    end

    return mode(game, function(game, args)
        game:get_hero():teleport(unpack(args))
    end, args)
end

local function now(game, f, ...)
    return f(game, ...)
end

local function later(_game, f, ...)
    local args = {...}
    return function(game)
        return f(game, unpack(args))
    end
end

local function multiple_actions(mode, game, str)
    local build_func = (mode == later)
    local functions = {} -- (mode == later and {})
    -- Skips ";;" or "".
    for section in string.gmatch(str, "([^;]+)") do
        local action, data = string.match(section, "^([^:]*):(.*)$")
        if not action then action = section end
        local func = actions[action]
        if func then
            local ret = func(mode, game, data)
            -- Should be same as checking ret.
            if build_func then
                table.insert(functions, ret)
            end
        else
            tools.error{"No such action: ", action}
        end
    end
    if build_func then
        return tools.broadcaster(functions)
    end
end

local function make_multiple_actions_hook(event_name)
    return function(engine, entity, value)
        local combined_action = multiple_actions(later, engine.game, value)
        engine:store_event(entity, event_name, function(self)
            combined_action(self:get_game())
        end)
    end
end

-- This is the most common one, so cache it once.
local on_activated = make_multiple_actions_hook"on_activated"

local function start_song(engine, entity, value)
    engine:store_event(entity, "on_activated", function(self)
        sol.audio.play_music(value)
    end)
end

local function set_hero_layer(engine, entity, value)
    local mode, num = string.match(value, "^([+=-]?)(%d+)$")
    num = tonumber(num)
    if not num then
        tools.error{"Bad value to set_hero_layer: ", value}
    elseif "" == mode or "=" == mode then
        engine:store_event(entity, "on_activated", function(self)
            self:get_map():get_hero():set_layer(num)
        end)
    elseif "+" == mode or "-" == mode then
        if "-" == mode then num = -num end
        engine:store_event(entity, "on_activated", function(self)
            local hero = self:get_map():get_hero()
            hero:set_layer(hero:get_layer() + num)
        end)
    end
end

local function set_solid_ground(engine, entity, value)
    if "save" == value then
        engine:store_event(entity, "on_activated", function(self)
            local hero = self:get_map():get_hero()
            hero:save_solid_ground()
        end)
    elseif "reset" == value then
        engine:store_event(entity, "on_activated", function(self)
            local hero = self:get_map():get_hero()
            hero:reset_solid_ground()
        end)
    else
        tools.error{"Bad value to set_solid_ground: ", value}
    end
end

-- Special variables get their values from these functions.
local special_variable_functions = {
    ["!is_hero_max_life"] = function(game, _entity)
        return game:get_life() == game:get_max_life()
    end,
}

-- This hook allows for dynamic selection of the dialog to run on NPCs.
local function dialog(engine, entity, value)
    local map = engine.map
    local game = map:get_game()

    -- Get the variable and raw-data:
    local var_name, data = value:match"^([^:]*):(.*)$"
    if nil == var_name then
        -- Is this a static dialog?
        if value:match"^[^:,=]+$" then
            -- Then skip all the dynamic parts and always start that.
            engine:store_event(entity, "on_interaction", function(self)
                settings.start_dialog(self:get_game(), value)
            end)
            return
        end
        -- Try the entity's name as a fallback.
        var_name = entity:get_name()
        if nil == var_name then
            tools.error{"No variable name for dialog hook (entity unamed)."}
        end
        data = value
    end

    -- Function to get the variable value.
    local get_value = special_variable_functions[var_name]
    if not get_value then
        function get_value(game, _entity) return game:get_value(var_name) end
    end

    -- Function to transform the value into a dialog_id.
    local cases = {}
    local default = nil
    for first, second in data:gmatch"([^,=]+)=?([^,=]*)" do
        if "" ~= second then
            -- if second == "false" then second = false end
            cases[first] = second
        elseif default then
            -- Issue some warning.
            print"[Lua Nova] Multiple dialog defaults provided, using first."
        else
            default = first
        end
    end
    local function get_dialog(value)
        return cases[tostring(value)] or default
    end

    -- Finally stitch the functions together to create the event.
    engine:store_event(entity, "on_interaction", function(self)
        local game = self:get_game()
        local value = get_value(game, self)
        local dialog_id = get_dialog(value)
        settings.start_dialog(game, dialog_id)
    end)
end

local function move_to_is_axis_reached(source, destination)
    if source < destination then
        return function(current) return destination <= current end
    elseif destination < source then
        return function(current) return current <= destination end
    else
        return function(current) return false end
    end
end

local function move_to_is_destination_reached(source, destination)
    local sx, sy, slayer = source:get_position()
    local dx, dy, dlayer = destination:get_position()
    local x_reached = move_to_is_axis_reached(sx, dx)
    local y_reached = move_to_is_axis_reached(sy, dy)
    return function(x, y) return x_reached(x) or y_reached(y) end
end

local function move_to(entity, destination, speed, callback)
    local move = sol.movement.create"straight"
    move:set_angle(entity:get_angle(destination))
    move:set_speed(speed)
    move:set_ignore_suspend(true)
    move:set_ignore_obstacles(true)
    local is_destination_reached = (
        move_to_is_destination_reached(entity, destination))
    function move:on_position_changed()
        local x, y = entity:get_position()
        if is_destination_reached(x, y) then
            self:stop()
            callback()
        end
    end
    move:start(entity)
end

local function open_door_animate(door, callback)
    door:open()
    local timer = sol.timer.start(door, 10, function()
        local is_opening = door:is_opening()
        if not is_opening then
            callback()
        end
        return is_opening
    end)
    -- Always make sure the timer can run.
    timer:set_suspended(false)
end

-- I would like this to be a modifier on any trigger hook.
local function open_when_camera(engine, entity, value)
    local function on_begin()
        local map = entity:get_map()
        local camera = map:get_camera()
        local game = entity:get_game()
        local source = camera:get_tracked_entity()
        local x, y, layer = source:get_position()
        local mover = map:create_custom_entity{
            x = x, y = y, layer = layer,
            width = 16, height = 16, direction = 0,
        }

        game:set_suspended(true)
        camera:start_tracking(mover)
        coroutine.yield(move_to, mover, entity, 128)
        coroutine.yield(open_door_animate, entity)
        coroutine.yield(move_to, mover, source, 128)
        camera:start_tracking(source)
        game:set_suspended(false)
        mover:remove()
    end
    trigger_common(engine, entity, value, tools.create_stepper(on_begin))
end

local function arena_foe(engine, entity, value)
    table.insert(engine.arenas[value].foes, entity)
end

local function arena_sensor(engine, entity, value)
    table.insert(engine.arenas[value].sensors, entity)
end

local function arena_entrance(engine, entity, value)
    table.insert(engine.arenas[value].entrances, entity)
end

-- Exports -------------------------------------------------------------------

hooks.mode = {now = now, later = later}

hooks.multiple_actions = multiple_actions

-- Hooks avalible on all entities:
hooks.entity = {
    enable_when = trigger_when(enable_common),
    enable_while = trigger_while(enable_common),
    disable_when = trigger_when(disable_common),
    disable_while = trigger_while(disable_common),
}

-- Hooks avalible on just these particular types:
hooks.door = {
    open_when = trigger_when(open_common),
    open_while = trigger_while(open_common),
    arena_entrance = arena_entrance,
    open_when_camera = open_when_camera,
}

hooks.dynamic_tile = {
    arena_entrance = arena_entrance,
}

hooks.enemy = {
    arena_foe = arena_foe,
}

hooks.npc = {
    dialog = dialog,
    on_interaction = make_multiple_actions_hook"on_interaction",
}

hooks.sensor = {
    start_song = start_song,
    arena_sensor = arena_sensor,
    set_hero_layer = set_hero_layer,
    set_solid_ground = set_solid_ground,
    on_activated = on_activated,
}

hooks.switch = {
    on_activated = on_activated,
}

return hooks
