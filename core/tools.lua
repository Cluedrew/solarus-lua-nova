-- Lua Nova Internal File
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- Common utilities and other tools for Lua Nova.

local tools = {}

-- System Tools Group:

-- This require is relative to the core/ directory.
local base_path = string.gsub(..., "/tools$", "/")
function tools.require(modname)
    return require(base_path .. modname)
end

function tools.error(data)
    local level = data.level or 1
    if 0 ~= level then level = level + 1 end

    data[1] = "[Lua Nova] " .. data[1]
    error(table.concat(data), level)
end

-- Assorted Tools Group:

local function post_step(s, f, ...)
    if f then
        local args = {...}
        local n = #args + 1
        args[n] = s
        f(unpack(args, 1, n))
    end
end

local function pre_step(w)
    return function(...) return post_step(pre_step(w), w(...)) end
end

function tools.create_stepper(f)
    return pre_step(coroutine.wrap(f))
end

-- Convert a string to a different Lua primitive if possible.
function tools.fromstring(str)
    if "nil" == str then
        return nil
    elseif "true" == str then
        return true
    elseif "false" == str then
        return false
    else
        -- I might have to add handling for signs.
        return tonumber(str) or str
    end
end

function tools.broadcaster(array)
    return function(...)
        for _, f in ipairs(array) do
            f(...)
        end
    end
end

return tools
