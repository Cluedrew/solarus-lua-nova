-- Lua Nova Internal File
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- This does not take any responsibility for actually drawing the dialog,
-- but adds effects around an existing dialog system.
--
-- To this end I fake there being an extra event (actually stored in here)
-- `game:on_dialog_stopped(dialog, status)`. This runs after the regular
-- events and callbacks. There is a special start_dialog and a
-- on_finished_dialog which are used to enable this.
--
-- And then there are a couple of hooks that relate to dialog.

-- Without modifying the engine or existing dialog scripts this is the best
-- solution I have been able to find.

local tools = require(string.gsub(..., "/dialog$", "/tools"))
local settings = tools.require"settings"
local hooks = tools.require"hooks"

local dialog = {}

local function try_action_string(game, action_string)
    if action_string then
        hooks.multiple_actions(hooks.mode.now, game, action_string)
    end
end

-- When a dialog finishes use it and its status to preform any effects.
function dialog.on_dialog_stopped(game, dialog_data, status)
    try_action_string(game, dialog_data.always)
    local if_string = dialog_data["if_" .. tostring(status)]
    try_action_string(game, if_string)
    if if_string then return end
    try_action_string(game, dialog_data.ifelse)
end

-- The next section is just structure to call the above.
local last_game, last_dialog = nil, nil

-- Save data for the callback.
local function on_dialog_finished(game, dialog_data)
    last_game, last_dialog = game, dialog_data
end

local function dialog_callback(status)
    if not last_dialog then
       error("No dialog detected.") 
    end

    local dialog_game, dialog_data = last_game, last_dialog
    last_game, last_dialog = nil, nil

    dialog.on_dialog_stopped(dialog_game, dialog_data, status)
end

-- A dialog that interacts with Lua Nova should be started with this.
local function start_dialog(game, dialog_id, info, callback)
    if callback then
        return game:start_dialog(dialog_id, info, function(status)
            callback(status)
            dialog_callback(status)
        end)
    else
        return game:start_dialog(dialog_id, info, dialog_callback)
    end
end

settings.start_dialog = start_dialog
dialog.start_dialog = start_dialog

-- TODO: Add disable and checks.
function dialog.enable_dialog(game)
    local register_event = settings.register_event
    if register_event then
        -- This function becomes one of however many registered.
        register_event(game, "on_dialog_finished", on_dialog_finished)
    elseif game.on_dialog_finished then
        -- Mix it with the current function.
        local old_dialog_finished = game.on_dialog_finished
        game.on_dialog_finished = function(...)
            old_dialog_finished(...)
            on_dialog_finished(...)
        end
    else
        -- Just set the callback (very unlikely).
        game.on_dialog_finished = on_dialog_finished
    end
end

return dialog
