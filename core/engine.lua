-- Lua Nova Internal File
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- The engine is responcible for scanning the map / entities / properties and
-- using that to create various new event functions.

local tools = require(string.gsub(..., "/engine$", "/tools"))
local hooks = tools.require"hooks"
local settings = tools.require"settings"

local engine = {}

local function emit_event(object, key, value)
    local register_event = settings.register_event
    if register_event then
        register_event(object, key, value)
    else
        object[key] = value
    end
end

local list_map_mt = {
    __index = function(self, key)
        local value = {}
        self[key] = value
        return value
    end
}

local list_map_map_mt = {
    __index = function(self, key)
        local value = setmetatable({}, list_map_mt)
        self[key] = value
        return value
    end
}

function engine.create(map)
    local self = {
        map = map,
        game = map:get_game(),
        events = setmetatable({}, list_map_map_mt),

        arenas = setmetatable({}, list_map_map_mt),
    }
    return setmetatable(self, engine)
end

engine.__index = engine

function engine.run(map)
    local self = engine.create(map)

    -- Process every entity in the map.
    for entity in map:get_entities() do
        local properties = entity:get_properties()
        local type_hooks
        if properties[1] then
            local entity_type = sol.main.get_type(entity)
            type_hooks = hooks[entity_type] or {}
        end

        -- Check every property to see if it is a hook.
        for i, kvp in ipairs(properties) do
            local key, value = kvp.key, kvp.value
            local hook = type_hooks[key] or hooks.entity[key]
            if hook then
                hook(self, entity, value)
            else
                -- This isn't a long term solution.
                print("Warning: no such hook " .. key)
            end
        end
    end

    self:complete_processing()
end

function engine:complete_processing()
    -- Anything that had to wait until we had found all the hooks.
    for name, data in pairs(self.arenas) do
        local arena_waiting = true
        local foe_count = #data.foes
        local entrances = data.entrances

        local function on_dead(self)
            foe_count = foe_count - 1
            if 0 == foe_count then
                for i, entrance in ipairs(entrances) do
                    if "door" == sol.main.get_type(entrance) then
                        entrance:open()
                    else
                        entrance:set_enabled(false)
                    end
                end
            end
        end
        for i, foe in ipairs(data.foes) do
            self:store_event(foe, "on_dead", on_dead)
        end
        local function on_activated(self)
            if arena_waiting then
                arena_waiting = false
                for i, entrance in ipairs(entrances) do
                    if "door" == sol.main.get_type(entrance) then
                        entrance:close()
                    else
                        entrance:set_enabled(true)
                    end
                end
            end
        end
        for i, sensor in ipairs(data.sensors) do
            self:store_event(sensor, "on_activated", on_activated)
        end
    end
    -- Export all the hooks as events.
    for entity, event_lists in pairs(self.events) do
        for event, list in pairs(event_lists) do
            emit_event(entity, event, tools.broadcaster(list))
        end
    end
end

function engine:store_event(entity, event_name, callable)
    table.insert(self.events[entity][event_name], callable)
end

return engine
