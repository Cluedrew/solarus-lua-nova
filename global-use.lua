-- Simple Lua Nova Interface
--
-- Copyright (C) 2020 Cluedrew Kenfar Ink
--
-- Lua Nova is free software: you can redistribute it and/or modify it under
-- the terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- Lua Nova is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with Lua Nova. If not, see <https://www.gnu.org/licenses/>.

-- If you want Lua Nova to effectively replace scripting, require this file
-- from main and delete all of the maps' on_started events.
--
-- Example:
--
-- require "lua-nova/global-use"

local lua_nova = require(string.gsub(..., "global[-]use$", "lua-nova"))

-- Check for a register_event from some multi-events script.
local register_event = sol.main.get_metatable("game").register_event
if register_event then
    lua_nova.register_event = register_event
else
    -- For compatability, we also wrap up a plain set.
    register_event = function(object, key, value)
        object[key] = value
    end
end

-- Enable dialog on each new game.
register_event(sol.main.get_metatable("game"), "on_started",
    lua_nova.enable_dialog)

-- Run full map processing when a map is started.
register_event(sol.main.get_metatable("map"), "on_started", lua_nova.run)

return
